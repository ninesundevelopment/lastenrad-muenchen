# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [3.5.0](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/compare/v3.4.0...v3.5.0) (2019-04-18)


### Features

* add error tracking ([fbf3258](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/commits/fbf3258))



# [3.4.0](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/compare/v3.3.5...v3.4.0) (2019-04-18)


### Features

* add error tracking ([d467dee](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/commits/d467dee))



## [3.3.5](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/compare/v3.3.4...v3.3.5) (2019-04-15)


### Bug Fixes

* wrong application title ([b4ecab5](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/commits/b4ecab5))



## [3.3.4](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/compare/v3.3.3...v3.3.4) (2019-04-15)



## [3.3.3](https://bitbucket.org/ninesundevelopment/lastenrad-muenchen/compare/v3.3.2...v3.3.3) (2019-04-15)
