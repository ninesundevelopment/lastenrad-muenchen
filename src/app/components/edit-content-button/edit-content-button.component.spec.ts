import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditContentButtonComponent} from './edit-content-button.component';

describe('EditContentButtonComponent', () => {
  let component: EditContentButtonComponent;
  let fixture: ComponentFixture<EditContentButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditContentButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditContentButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
