import {Component} from '@angular/core';
import {EditorService} from '@app/services/editor/editor.service';

declare const require: any;

let CodeMirror = require('codemirror');
let beautify_html = require('js-beautify').html;

@Component({
    selector: 'editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class EditorComponent {

    public visible: boolean;
    public content: string;

    private codeMirror: any = null;

    constructor(
        private editorSrv: EditorService,
    ) {
        this.editorSrv.visible.subscribe((visible) => {
            this.visible = visible;
            this.startEditor();
        });

        this.editorSrv.content.subscribe((content) => {
            this.content = content;
            this.startEditor();
        });
    }

    startEditor() {
        if (this.content === null ||
            this.content === undefined ||
            !this.visible) {

            return;
        }

        this.content = beautify_html(this.content);

        const textarea: HTMLTextAreaElement = document.getElementById('codemirror') as HTMLTextAreaElement;
        textarea.innerHTML = this.content;

        this.codeMirror = CodeMirror.fromTextArea(textarea, {
            value: this.content,
            lineNumbers: true,
            theme: 'material',
            mode: 'markdown',
            lineWrapping: true,
        });

        const refreshTimer = window.setInterval(() => {
            if (!this.codeMirror.hasFocus()) {
                this.codeMirror.refresh();
                this.codeMirror.focus();
                this.codeMirror.getDoc().setValue(this.content);
            } else {
                window.clearInterval(refreshTimer);
            }
        }, 25);
    }

    saveText() {
        let content = this.codeMirror.getDoc().getValue();
        content = content ? content : '&nbsp;';

        this.codeMirror.toTextArea();
        this.visible = false;
        this.content = ' ';
        const textarea: HTMLTextAreaElement = document.getElementById('codemirror') as HTMLTextAreaElement;
        textarea.innerHTML = this.content;
        this.editorSrv.save(content);
    }

    closeEditor() {
        this.codeMirror.toTextArea();
        this.visible = false;
        this.content = ' ';
        const textarea: HTMLTextAreaElement = document.getElementById('codemirror') as HTMLTextAreaElement;
        textarea.innerHTML = this.content;
        this.editorSrv.discard();
    }
}
