import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './components/app/app.component';
import {IndexComponent} from './pages/index/index.component';
import {HeaderComponent} from './components/header/header.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {SidebarService} from './services/sidebar/sidebar.service';
import {ScrollService} from './services/scroll/scroll.service';
import {CreditsComponent} from './pages/credits/credits.component';
import {WindowScrollingService} from './services/window-scrolling/window-scrolling.service';
import {PageSectionComponent} from './components/page-section/page-section.component';
import {UICarouselComponent} from './components/ui-carousel/ui-carousel.component';
import {UICarouselItemComponent} from './components/ui-carousel-item/ui-carousel-item.component';
import {DotsComponent} from './components/dots/dots.component';
import {SwiperDirective} from './directives/swiper.directive';
import {UILazyloadDirective} from './directives/ui-lazy-load.directive';
import {UrlService} from './services/url/url.service';
import {SafePipe} from './pipes/safe.pipe';
import {HttpClientModule} from '@angular/common/http';
import {OfflineComponent} from './pages/offline/offline.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {ScrollToTopComponent} from './components/scroll-to-top/scroll-to-top.component';
import {BookComponent} from './pages/book/book.component';
import {HowItWorksComponent} from './pages/how-it-works/how-it-works.component';
import {BecomeStationComponent} from './pages/become-station/become-station.component';
import {AdfcComponent} from './pages/adfc/adfc.component';
import {DonationsComponent} from './pages/donations/donations.component';
import {PressComponent} from './pages/press/press.component';
import {ModalComponent} from './components/modal/modal.component';
import {ModalService} from './services/modal.service';
import {UserCardComponent} from './components/user-card/user-card.component';
import {ImprintComponent} from './pages/imprint/imprint.component';
import {DataProtectionComponent} from './pages/data-protection/data-protection.component';
import {TermsOfUseComponent} from './pages/terms-of-use/terms-of-use.component';
import {UserService} from './services/user/user.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OrderService} from './services/order/order.service';
import {StationService} from './services/station/station.service';
import {StationComponent} from './components/station/station.component';
import {DayComponent} from './components/day/day.component';
import {CookieService} from 'ngx-cookie-service';
import {CheckboxComponent} from './components/checkbox/checkbox.component';
import {EditContentButtonComponent} from './components/edit-content-button/edit-content-button.component';
import {ContentService} from './services/content/content.service';
import {EditorComponent} from './components/editor/editor.component';
import {EditorService} from './services/editor/editor.service';
import {CodemirrorModule} from '@ctrl/ngx-codemirror';
import 'codemirror/mode/markdown/markdown';
import {HtmlPipe} from './pipes/html/html.pipe';
import {ActivateComponent} from './pages/activate/activate.component';
import {RegisterComponent} from './components/register/register.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {CommonModule, registerLocaleData} from "@angular/common";
import localeDe from '@angular/common/locales/de';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatCheckboxModule,
  MatAutocompleteModule
} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

registerLocaleData(localeDe);

@NgModule({
  bootstrap: [AppComponent],
  entryComponents: [
    UserCardComponent,
    RegisterComponent,
  ],
  declarations: [
    AppComponent,
    IndexComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    CreditsComponent,
    PageSectionComponent,
    UICarouselComponent,
    UICarouselItemComponent,
    DotsComponent,
    SwiperDirective,
    UILazyloadDirective,
    SafePipe,
    OfflineComponent,
    NotFoundComponent,
    ScrollToTopComponent,
    BookComponent,
    HowItWorksComponent,
    BecomeStationComponent,
    AdfcComponent,
    DonationsComponent,
    PressComponent,
    ModalComponent,
    UserCardComponent,
    ImprintComponent,
    DataProtectionComponent,
    TermsOfUseComponent,
    StationComponent,
    DayComponent,
    CheckboxComponent,
    EditContentButtonComponent,
    EditorComponent,
    HtmlPipe,
    ActivateComponent,
    RegisterComponent,
    ProfileComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CodemirrorModule,

    MatDialogModule,
    MatInputModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatFormFieldModule,
  ],
  providers: [
    SidebarService,
    ScrollService,
    WindowScrollingService,
    UrlService,
    ModalService,
    UserService,
    OrderService,
    StationService,
    CookieService,
    ContentService,
    EditorService
  ],
})
export class AppModule {
}

