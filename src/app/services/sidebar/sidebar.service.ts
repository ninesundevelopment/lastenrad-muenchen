import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class SidebarService {

    public visible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() {
    }

}
