import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class ScrollService {

    public scrollTop: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor() {
    }

}
