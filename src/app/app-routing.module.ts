import {IndexComponent} from './pages/index/index.component';
import {CreditsComponent} from './pages/credits/credits.component';
import {OfflineComponent} from './pages/offline/offline.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {BookComponent} from './pages/book/book.component';
import {HowItWorksComponent} from './pages/how-it-works/how-it-works.component';
import {BecomeStationComponent} from './pages/become-station/become-station.component';
import {AdfcComponent} from './pages/adfc/adfc.component';
import {DonationsComponent} from './pages/donations/donations.component';
import {PressComponent} from './pages/press/press.component';
import {ImprintComponent} from './pages/imprint/imprint.component';
import {DataProtectionComponent} from './pages/data-protection/data-protection.component';
import {TermsOfUseComponent} from './pages/terms-of-use/terms-of-use.component';
import {ActivateComponent} from './pages/activate/activate.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {Route, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {environment} from "@env/environment";

export const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home',
  },
  {
    path: 'home',
    component: IndexComponent,
  },
  {
    path: 'book',
    component: BookComponent,
  },
  {
    path: 'how-it-works',
    component: HowItWorksComponent,
  },
  {
    path: 'become-station',
    component: BecomeStationComponent,
  },
  {
    path: 'adfc',
    component: AdfcComponent,
  },
  {
    path: 'donations',
    component: DonationsComponent,
  },
  {
    path: 'press',
    component: PressComponent,
  },
  {
    path: 'profile',
    component: ProfileComponent,
  },
  {
    path: 'credits',
    component: CreditsComponent,
  },
  {
    path: 'imprint',
    component: ImprintComponent,
  },
  {
    path: 'data-protection',
    component: DataProtectionComponent,
  },
  {
    path: 'terms-of-use',
    component: TermsOfUseComponent,
  },
  {
    path: '404',
    component: NotFoundComponent,
  },
  {
    path: 'offline',
    component: OfflineComponent,
  },
  {
    path: 'activate/:id',
    component: ActivateComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: !environment.production,
      enableTracing: false,
      scrollPositionRestoration: 'disabled',
      anchorScrolling: 'enabled',
      urlUpdateStrategy: 'eager',
    }),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}
