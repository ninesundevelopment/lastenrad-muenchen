# TAW

A lightweight boilerplate to get started with TypeScript, Angular5 and Webpack

### Setup
##### Prerequisites
* node.js
* npm
* gulp

_Use latest version if not specified_

### Installing
* Get the Repository: `git clone git@bitbucket.org:NineSunDev/taw-base.git`
* Install it `npm i`
* Done.

### Recommended Folder-Structure
``` 
    /
    |_ dist/                // Output
    |_ karma/               // Config
    |_ node_modules/        // Node Libraries
    |_ resources/           // Html, Fonts, Images, Sounds, etc.
    |_ src/                 // Sources
     |_ components/
      |_ app/
       |_ app.component.html
       |_ app.component.scss
       |_ app.component.ts
       |_ app.component.spec.ts
     |_ pages/
      |_ index/
       |_ index.page.html
       |_ index.page.scss
       |_ index.page.ts
       |_ index.page.spec.ts
     |_ provider/
     |_ pipes/
     |_ stylesheets/
     |_ .../
    |_ typings/             // Config
    |_ webpack/             // Config
    |_ gulpfile.js
    |_ package.json
    |_ README.md
    |_ tsconfig.json
    |_ tslint.json
```

### Development & Building
* One-time-build: `npm run dist`
* Dev build + Live reload: `npm start`
* Testing: `npm test`
* Code Generation: 
    
    `ng g[enerate] c[omponent] PATH/NAME`
    
    `ng g[enerate] p[rovider] PATH/NAME`
    
    ...

### Contribution
* Write tests if necessary
* Comment your code
* Make a merge request
* Have my thanks :)


### Owner
* Owner: Nico S&auml;nger (NineSun Development)

    [nico.saenger@ninesun.de](nico.saenger@ninesun.de)

### License
[![cc-by-sa](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
